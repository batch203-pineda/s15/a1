let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming' ];

let workAddress = {
    houseNumber: '32',
    street: 'Washington',
    city: 'Lincoln',
    state: 'Nebraska'
}


console.log("First Name: ",firstName);
console.log("Last Name: ",lastName);
console.log("Age: ",age);
console.log("Hobbies: ",hobbies);
console.log("Work Address: ",workAddress);

let fullName = "Steve Rogers";
let currentAge = "40";
let friens = ['Tony', 'Bruce', 'Thor', 'Natasha', 'Clint', 'Nick'];

let fullProfile = {
    username: 'captain_america',
    fullName: 'Steve Rogers',
    age: '40',
    isActive: false
}

let bestFriend = "Bucky Barnes";
let articOcean = "Artic Ocean";

console.log("My full name is: ",fullName);
console.log("My current age is: ",age);
console.log("My Friends are: ",friens);
console.log("My Full Profile: ", fullProfile);
console.log("My bestfriend is: ",bestFriend);
console.log("I was found frozen in: ",articOcean);